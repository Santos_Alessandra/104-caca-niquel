package Jogo211;

public class Carta {
	public String rank;
	public String naipe;
	
	public Carta(String rank, String naipe){
		this.rank = rank;
		this.naipe = naipe;
	}
	
	public String toString() {
		return rank + " de " + naipe;
	}
}
