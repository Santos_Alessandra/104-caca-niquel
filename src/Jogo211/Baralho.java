package Jogo211;

import java.util.ArrayList;
import java.util.Random;

public class Baralho {
	public String[] ranks = {"As", "2", "3"};
	public String[] naipes = {"Paus", "Ouros", "Copas", "Espadas"};

	ArrayList<Carta> cartas = new ArrayList<>();
	
	public Baralho(){
		for(String rank: ranks) {
			for(String naipe: naipes) {
				cartas.add(new Carta(rank, naipe));
			}
		}
	}
	
	public void embaralhar() {
		ArrayList<Carta> cartasEmbaralhadas = new ArrayList<>();
		Random random = new Random();
		
		while(cartas.size() > 0) {
			int sorteio = random.nextInt(cartas.size());
			
			Carta cartaSorteada = cartas.remove(sorteio);
			
			cartasEmbaralhadas.add(cartaSorteada);
		}
		
		cartas = cartasEmbaralhadas;
	}
	
	public String toString() {
		StringBuilder builder = new StringBuilder();
		
		for(Carta carta: cartas) {
			builder.append(carta.toString() + "\n");
		}
		
		return builder.toString();
	}
	
}
 